import React from "react";
import Helmet from 'react-helmet';
const Main = (props) => {
    return(
        <body className="hold-transition sidebar-mini layout-fixed">
            <Helmet titleTemplate={`%s | Bypass`} /> 
            <div className="wrapper">
                <nav className="main-header navbar navbar-expand navbar-white navbar-light navbar-sidebar">
                    <div className="col-md-12">
                        <div className="row">
                            <div className="col-md-3">
                                <img src="./assets/images/Bypass_plate.png" className="apple-img"/>
                            </div>
                       
                            <div className="col-md-4">
                                <div className="input-group input-group-sm">
                                      <input className="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search" />
                                        <div className="input-group-append">
                                        <button className="btn btn-navbar" type="submit">
                                          <i className="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-1">
                            </div>
                           <div className="col-md-1">
                            <button type="button" className="btn btn-block btn-outline-info btn-sm">Create a post</button>
                          </div>
                          <div className="col-md-1">
                            <button type="button" className="btn btn-block btn-outline-info btn-sm">Sign in/Register</button>
                          </div>
                          <div className="col-md-1">
                            <button type="button" className="btn btn-block btn-primary btn-sm">Browser Tasks</button>
                          </div>
                        </div>
                    </div>
                </nav>
                    <div className="content-wrapper">
                        <section className="content">
                            <div className="row">
                                <div className="col-md-10 offset-md-1">
                                  <div className="card card-body">
                                        <div className="col-md-12">
                                            <div className="row">
                                                <div className="col-md-4">
                                                    <br />
                                                    <div className="title-design">
                                                        <p className="paragraph"><b>Get starting a business done</b></p><br />
                                                        <span className="title2-size">
                                                        <p>Logo design</p>
                                                        <p>Build a website</p>
                                                        <p>Accounting</p>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="col-md-4">
                                                </div>
                                                <div className="col-md-4">
                                                    <img src="./assets/images/img.jpeg" className="img-size"/>
                                                </div>
                                            </div>
                                        </div>
                                  </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6 offset-md-2">
                                    <div className="input-group">
                                          <input className="form-control form-control-lg" type="search" placeholder="Form control Lg" aria-label="Search" />
                                    </div>
                                </div>
                                <div className="col-md-2">
                                    <button type="button" className="btn btn-block btn-outline-info bt-lg overviewBtn">Let get Started</button>
                              </div>
                            </div>
                            <div className="row marginTop">
                                <div className="col-md-2 offset-md-3">
                                    <button type="button" className="btn btn-block btn-outline-info bt-lg">Packer and Movers</button>
                                </div>
                                <div className="col-md-2">
                                    <button type="button" className="btn btn-block btn-outline-info bt-lg">Home Reparis</button>
                              </div>
                              <div className="col-md-1">
                                    <button type="button" className="btn btn-block btn-outline-info bt-lg">Move</button>
                              </div>
                              <div className="col-md-1">
                                    <button type="button" className="btn btn-block btn-outline-info bt-lg">See More</button>
                              </div>
                            </div>
                        </section>
                    </div>

                <footer className="main-footer">
                    <div className="row">
                        <div className="col-md-3 offset-md-1">
                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                            <i className="fa fa-star yellow" aria-hidden="true"></i>
                            <i className="fa fa-star" aria-hidden="true"></i>
                        <br/>
                        1.1 Million Reviews
                        </div>
                        <div className="col-md-3">
                            <p><img src="./assets/images/ikea.png" className="apple-img"/>
                            &nbsp;partnered with IKEA</p>
                        </div>
                        <div className="col-md-2">
                        </div>
                        <div className="col-md-3">
                        <img src="./assets/images/apple.png" className="apple-img"/>&nbsp;
                         <img src="./assets/images/google.png" className="apple-img"/>
                        </div>
                    </div>
                    <div className="row float-right">
                        <p>Policies</p>&nbsp;
                        <p>About Us</p>&nbsp;
                        <p>Conatct Us</p>&nbsp;
                    </div>
                </footer>
                
                
            </div>
        </body>
    )
}
export default Main;
